package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ChildEntity childEntity = new ChildEntity("child");
        ParentEntity parent = new ParentEntity("parent");
        childEntity.setParentEntity(parent);

        flushAndClear(entityManager -> parentEntityRepository.save(parent));
        flushAndClear(entityManager -> childEntityRepository.save(childEntity));

        ChildEntity queryChild = getData(childEntityRepository.findById(childEntity.getId()));
        Assertions.assertEquals("parent", queryChild.getParentEntity().getName());
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ChildEntity childEntity = new ChildEntity("child");
        ParentEntity parent = new ParentEntity("parent");
        childEntity.setParentEntity(parent);
        flushAndClear(entityManager -> parentEntityRepository.save(parent));
        flushAndClear(entityManager -> childEntityRepository.save(childEntity));

        childEntity.setParentEntity(null);
        flushAndClear(em -> childEntityRepository.save(childEntity));

        ChildEntity queryChild = getData(childEntityRepository.findById(childEntity.getId()));
        Assertions.assertNull(queryChild.getParentEntity());
        ParentEntity parentEntity = getData(parentEntityRepository.findById(parent.getId()));
        Assertions.assertNotNull(parentEntity);
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ChildEntity childEntity = new ChildEntity("child");
        ParentEntity parent = new ParentEntity("parent");
        childEntity.setParentEntity(parent);
        flushAndClear(entityManager -> parentEntityRepository.save(parent));
        flushAndClear(entityManager -> childEntityRepository.save(childEntity));

        flushAndClear(em -> childEntityRepository.delete(childEntity));
        flushAndClear(em -> parentEntityRepository.delete(parent));


        Optional<ChildEntity> optionalChildEntity = childEntityRepository.findById(childEntity.getId());
        Optional<ParentEntity> optionalParentEntity = parentEntityRepository.findById(parent.getId());
        Assertions.assertFalse(optionalChildEntity.isPresent());
        Assertions.assertFalse(optionalParentEntity.isPresent());
        // --end-->
    }

    private <T> T getData(Optional<T> optionalT){
        Assertions.assertTrue(optionalT.isPresent());
        return optionalT.get();
    }
}
